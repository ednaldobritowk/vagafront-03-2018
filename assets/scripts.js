
	/* ================================
===   Menu   ====
=================================== */

$( ".btn-menu" ).on( "click", function() {
	$(this).toggleClass('active');
	$('div.menu').toggleClass('active');


	if($('body').hasClass('no-scroll'))
		$('body').removeClass('no-scroll');

	else
		$('body').toggleClass('no-scroll');

	return false;

});


	/* ================================
===   Vitrine Home   ====
=================================== */


$('#owlHome').owlCarousel({
	loop:true,
	margin:0,
	nav:true,
	navText:false,
	items:1,
	autoplay:5000,
	dotsContainer: '#paginacao',
})



/* ================================
===   Pop Up automático   ====
=================================== */

$(document).ready(function() {
  
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
  
    $('#mask').css({'width':maskWidth,'height':maskHeight});
 
    $('#mask').fadeIn(1000);  
    $('#mask').fadeTo("slow",0.8);
  
    //Get the window height and width
    var winH = $(window).height();
    var winW = $(window).width();
              
    $('#box').css('top',  winH/2-$('#box').height()/2);
    $('#box').css('left', winW/2-$('#box').width()/2);
  
    $('#box').fadeIn(2000); 
  
  $('.window .close').click(function (e) {
    e.preventDefault();
    
    $('#mask').hide();
    $('.window').hide();
  });   
  
  $('#mask').click(function () {
    $(this).hide();
    $('.window').hide();
  });  

  $('.btnEnvia').click(function(){
  	var nome = $
  	('input[name="nome"]').val();
  		$('.formulaCadastro').hide();
  		$('.mensagemSucesso').show();
  		$('.textNome').text(nome);
  });   
  
});